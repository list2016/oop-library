package BookAndLibrary;

import java.util.Arrays;

public class Library {

    private Book[] books;

    public Library(){
        books = new Book[3];
    }

    public Book getBookByIndex(int index) {
        return books[index];
    }

    public void setBookByIndex(Book book, int index) {
        books[index] = book;
    }

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        if(this.books.length == books.length)
            this.books = books;
    }

    public int getBooksLength() {
        return books.length;
    }

    @Override
    public String toString() {
        return "Библиотека " + "книг = " + Arrays.toString(books);
    }
}
