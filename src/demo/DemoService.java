package demo;

import BookAndLibrary.Book;
import BookAndLibrary.Library;
import libService.*;

public class DemoService  {

    private ILibraryService libraryService;
    private Library library;

    public DemoService(ILibraryService libraryService, Library library) {
        this.libraryService = libraryService;
        this.library = library;
    }

    public void execute(){
        checkAdd();
        checkDelete();
        checkSearch();
    }

    public void checkAdd() {
        System.out.println("Добавление книг:");
        libraryService.addBook(library, new Book("book1", "author1"));
        libraryService.addBook(library, new Book("book2", "author2"));
        libraryService.addBook(library, new Book("book3", "author3"));
        System.out.println(library);
        if (!libraryService.addBook(library, new Book("book4", "author4")))
            System.out.println("Библиотека заполнена");
    }

    public void checkDelete() {
        System.out.println("Удаление книги book3");
        Book book = libraryService.FindBookByName(library, "book3");
        libraryService.deleteBook(library, book);
        System.out.println(library);
    }

    public void checkSearch() {
        System.out.println("Поиск книг по названию:");
        Book book = libraryService.FindBookByName(library, "book1");
        System.out.println(book);
        book = libraryService.FindBookByName(library, "book2");
        System.out.println(book);
    }
}
