import BookAndLibrary.Library;
import demo.DemoService;
import libService.LibraryService;

public class main {
    public static void main(String[] args) {
        new DemoService(new LibraryService(),new Library()).execute();
    }
}
