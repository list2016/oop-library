package libService;

import BookAndLibrary.Book;
import BookAndLibrary.Library;

public interface ILibraryService {

    boolean addBook(Library library, Book book);

    boolean deleteBook(Library library, Book book);

    Book FindBookByName (Library library, String search);

}
