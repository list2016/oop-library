package libService;

import BookAndLibrary.Book;
import BookAndLibrary.Library;

public class LibraryService implements ILibraryService {

    @Override
    public boolean addBook(Library library, Book book) {
        for (int i = 0; i < library.getBooksLength(); i++)
            if (library.getBookByIndex(i) == null) {
                library.setBookByIndex(book, i);
                return true;
            }
        return false;
    }

    @Override
    public boolean deleteBook(Library library, Book book) {
        for (int i = 0; i < library.getBooksLength(); i++)
            if(library.getBookByIndex(i).equals(book)) {
                library.setBookByIndex(null, i);
                return true;
            }
        return false;
    }

    @Override
    public Book FindBookByName(Library library, String name) {
        Book book = new Book(name);
        for (int i = 0; i < library.getBooksLength(); i++)
            if(library.getBookByIndex(i).equals(book))
                return library.getBookByIndex(i);
        return null;
    }

}
